# Vagrant CentOS 7 Desktop

Installs a CentOS 7 (GNOME) Desktop in [libvirt](https://libvirt.org/).

* Configure [Vagrant](https://developer.hashicorp.com/vagrant/tutorials/getting-started)
* git clone this repository in `~/Vagrant` and
* run `vagrant up`
